package com.revyakinpa.tinkofffintech

import com.google.gson.annotations.SerializedName

data class StudentGroup(
        @SerializedName("grades")
        val students: List<Students>
)

data class Students(
        @SerializedName("student")
        var name: String,
        val grades: List<Grade>
)

data class Grade(
        val mark: Double
)