package com.revyakinpa.tinkofffintech

import android.app.IntentService
import android.content.Intent
import android.provider.ContactsContract
import androidx.localbroadcastmanager.content.LocalBroadcastManager

class ContactIntentService : IntentService("ContactSerivce") {

    private var contactModelArrayList: ArrayList<ContactModel>? = null

    override fun onHandleIntent(p0: Intent?) {

        contactModelArrayList = ArrayList()

        val phones = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC")
        var i = 1

        while (phones?.moveToNext()!!) {
            val name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))

            /**
             * Наверняка у пользователей будет id, а также баллы будут известны сразу.
             * Допустим что они приходят сразу с именем, поэтому присваиваем id и score здесь
             */
            val contactModel = ContactModel(name, i, (0..100).random())
            i += 1
            contactModelArrayList!!.add(contactModel)
        }
        phones.close()

        val responseIntent = Intent()
        responseIntent.action = ACTION
        responseIntent.addCategory(Intent.CATEGORY_DEFAULT)
        responseIntent.putParcelableArrayListExtra(KEY_OUT, contactModelArrayList)
        LocalBroadcastManager.getInstance(this).sendBroadcast(responseIntent)

    }

    companion object {
        const val ACTION = "com.revyakinpa.tinkofffintech.RESPONSE"
        const val KEY_OUT = "KEY_OUT"
    }
}