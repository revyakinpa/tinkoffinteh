package com.revyakinpa.tinkofffintech

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContactModel(var name: String, var id: Int, var score: Int) : Parcelable
