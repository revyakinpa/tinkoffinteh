package com.revyakinpa.tinkofffintech.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.revyakinpa.tinkofffintech.NetManager
import com.revyakinpa.tinkofffintech.SharedPreferencesManager

class SignInViewModel(app: Application) : AndroidViewModel(app) {

    private val model = UserModel(NetManager(getApplication()), SharedPreferencesManager.newManager(getApplication()))
    val loginState = model.loginState
    val tokenState = model.tokenState

    val isLoading = model.isLoading

    fun signIn(email: String, password: String) {
        model.signin(email, password)
    }

    fun hasToken() {
        model.hasToken()
    }

}