package com.revyakinpa.tinkofffintech.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.revyakinpa.tinkofffintech.NetManager
import com.revyakinpa.tinkofffintech.SharedPreferencesManager
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*

class UserModel(private val netManager: NetManager, private val sharedPreferencesManager: SharedPreferencesManager) {

    private val mutableLoginState = MutableLiveData<SignInResult>()
    val loginState: LiveData<SignInResult> = mutableLoginState

    private val mutableTokenState = MutableLiveData<TokenResult>()
    val tokenState: LiveData<TokenResult> = mutableTokenState

    val isLoading = MutableLiveData<Boolean>()


    fun hasToken() {
        if (sharedPreferencesManager.containsCookie()) {
            /**
             * Если токен актуален, то входим, иначе просим пользователя авторизоваться еще раз
             */
            val dateFormat = SimpleDateFormat("E, dd-MMM-yyyy HH:mm:ss z", Locale.ENGLISH)
            val expires = dateFormat.parse(sharedPreferencesManager.getExpires())
            val currentDate = dateFormat.parse(dateFormat.format(Date()))
            if (expires > currentDate) {
                mutableTokenState.value = TokenResult(true, "Токен актуален")
            } else {
                sharedPreferencesManager.revomeCookies()
                mutableTokenState.value = TokenResult(false, "Токен устарел")
            }

        } else {
            mutableTokenState.value = TokenResult(false, "Токен отсутствует")
        }
    }

    fun signin(email: String, password: String) {
        netManager.isConnectedToInternet?.let {
            if (it) {
                isLoading.value = true
                userService.signin(SignInBody(email, password)).enqueue(object : Callback<Void> {
                    override fun onFailure(call: Call<Void>, t: Throwable) {
                        isLoading.value = false
                        mutableLoginState.value = SignInResult(false, "Ошибка запроса")
                    }

                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                        var msg = "Успех"
                        if (response.isSuccessful) {
                            val result = response.headers().get("Set-Cookie")
                            sharedPreferencesManager.saveCookie(getCookie(result!!), getExpires(result))
                        } else {
                            msg = "Введен неверный email или пароль"
                        }
                        isLoading.value = false
                        mutableLoginState.value = SignInResult(response.isSuccessful, msg)
                    }
                })
            } else {
                mutableLoginState.value = SignInResult(false, "Нет подключения к интернету")
            }
        }

    }

    private fun getCookie(s: String): String {
        return s.split("; ")[0]
    }


    private fun getExpires(s: String): String {
        return s.substring(s.indexOf("expires", 0)).split("; ")[0].split("=")[1]
    }


    companion object {
        private val okHttpClient = OkHttpClient.Builder()
                .build()

        private val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .baseUrl("https://fintech.tinkoff.ru/api/")
                .build()

        val userService: UserService = retrofit.create(UserService::class.java)
    }
}


typealias SignInResult = Pair<Boolean, String>
typealias TokenResult = Pair<Boolean, String>