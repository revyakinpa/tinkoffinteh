package com.revyakinpa.tinkofffintech.login

import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*
import androidx.lifecycle.Observer
import com.revyakinpa.tinkofffintech.MainActivity
import android.text.TextUtils




class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.revyakinpa.tinkofffintech.R.layout.activity_login)


        val viewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)

        viewModel.hasToken()

        viewModel.loginState.observe(this, Observer<SignInResult> {
            when (it.first) {
                true -> openMain()
                false -> onError(it.second)
            }
        })


        viewModel.tokenState.observe(this, Observer<TokenResult> {
            if (it.first) openMain()
        })

        viewModel.isLoading.observe(this, Observer<Boolean> {
            if (it) {
                button_signin.visibility = View.GONE
                progressBar_signin.visibility = View.VISIBLE
            } else {
                button_signin.visibility = View.VISIBLE
                progressBar_signin.visibility = View.GONE
            }
        })

        button_signin.setOnClickListener {
            val email = text_email.text.toString()
            val psw = text_password.text.toString()
            when {
                email == "" -> Toast.makeText(this, "Ты забыл ввести email", Toast.LENGTH_SHORT).show()
                psw == "" -> Toast.makeText(this, "Ты забыл ввести пароль", Toast.LENGTH_SHORT).show()
                !isValidEmail(email) -> Toast.makeText(this, "Ты ввел невалидный email", Toast.LENGTH_SHORT).show()
                else -> viewModel.signIn(email, psw)
            }

        }
    }

    private fun openMain() {
        val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun onError(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    fun isValidEmail(target: CharSequence): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }


}
