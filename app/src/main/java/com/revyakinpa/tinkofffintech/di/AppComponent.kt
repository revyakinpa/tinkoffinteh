package com.revyakinpa.tinkofffintech.di

import android.app.Application
import com.revyakinpa.tinkofffintech.TaskFragment
import com.revyakinpa.tinkofffintech.course.CourseFragment
import com.revyakinpa.tinkofffintech.course.performance.CoursePerformanceFragment
import com.revyakinpa.tinkofffintech.course.rating.CourseRatingFragment
import com.revyakinpa.tinkofffintech.events.EventFragment
import com.revyakinpa.tinkofffintech.events.EventListFragment
import com.revyakinpa.tinkofffintech.lecture.LectureFragment
import com.revyakinpa.tinkofffintech.performance.PerformanceFragment
import com.revyakinpa.tinkofffintech.profile.ProfileFragment
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    AndroidInjectionModule::class,
    AndroidSupportInjectionModule::class,
    NetModule::class,
    ModelModules::class,
    FragmentModule::class,
    ViewModelModule::class])
interface AppComponent {

    fun inject(fintechApplication: FintechApplication)

    fun inject(app: Application)

    fun inject(lectureFragment: LectureFragment)

    fun inject(performanceFragment: PerformanceFragment)

    fun inject(taskFragment: TaskFragment)

    fun inject(eventFragment: EventFragment)

    fun inject(eventListFragment: EventListFragment)

    fun inject(profileFragment: ProfileFragment)

    fun inject(courseFragment: CourseFragment)

    fun inject(coursePerformanceFragment: CoursePerformanceFragment)

    fun inject(courseRatingFragment: CourseRatingFragment)
}