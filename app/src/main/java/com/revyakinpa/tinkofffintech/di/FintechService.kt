package com.revyakinpa.tinkofffintech.di

import com.revyakinpa.tinkofffintech.Homeworks
import com.revyakinpa.tinkofffintech.StudentGroup
import com.revyakinpa.tinkofffintech.course.Courses
import com.revyakinpa.tinkofffintech.events.Events
import com.revyakinpa.tinkofffintech.login.SignInBody
import com.revyakinpa.tinkofffintech.profile.Profile
import com.revyakinpa.tinkofffintech.profile.ResponseUser
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.*

interface FintechService {
    @GET("course/android_spring_2019/homeworks")
    fun getHomeworks(@Header("Cookie") cookie: String): Call<Homeworks>

    @POST("signin")
    fun signin(@Body body: SignInBody): Call<Void>

//    @GET("course/android_spring_2019/grades")
//    fun getStudentsGroups(@Header("Cookie") cookie: String): Call<List<StudentGroup>>

    @GET("course/android_spring_2019/grades")
    fun getStudentsGroupsRX(@Header("Cookie") cookie: String): Observable<List<StudentGroup>>

    @GET("user")
    fun getProfile(@Header("Cookie") cookie: String): Call<ResponseUser>

    @GET("user")
    fun getProfileRX(@Header("Cookie") cookie: String): Single<ResponseUser>

    @GET("calendar/list/event")
    fun getEvents(): Call<Events>

    @GET("connections")
    fun getCourse(@Header("Cookie") cookie: String): Call<Courses>

    @GET("course/{course_id}/grades")
    fun getStudentsGroups(@Header("Cookie") cookie: String, @Path(value = "course_id", encoded = true) course_id: String): Call<List<StudentGroup>>
}
