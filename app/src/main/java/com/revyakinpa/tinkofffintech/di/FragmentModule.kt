package com.revyakinpa.tinkofffintech.di

import com.revyakinpa.tinkofffintech.course.CourseFragment
import com.revyakinpa.tinkofffintech.course.performance.CoursePerformanceFragment
import com.revyakinpa.tinkofffintech.course.rating.CourseRatingFragment
import com.revyakinpa.tinkofffintech.events.EventFragment
import com.revyakinpa.tinkofffintech.events.EventListFragment
import com.revyakinpa.tinkofffintech.lecture.LectureFragment
import com.revyakinpa.tinkofffintech.performance.PerformanceFragment
import com.revyakinpa.tinkofffintech.profile.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    internal abstract fun contributeLectureFragment(): LectureFragment

    @ContributesAndroidInjector
    internal abstract fun contributePerformanceFragment(): PerformanceFragment

    @ContributesAndroidInjector
    internal abstract fun contributeEventFragment(): EventFragment

    @ContributesAndroidInjector
    internal abstract fun contributeEventListFragment(): EventListFragment

    @ContributesAndroidInjector
    internal abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCourseFragment(): CourseFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCoursePerformanceFragment(): CoursePerformanceFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCourseRatingFragment(): CourseRatingFragment
}
