package com.revyakinpa.tinkofffintech.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.revyakinpa.tinkofffintech.course.CourseModel
import com.revyakinpa.tinkofffintech.course.CourseViewModel
import com.revyakinpa.tinkofffintech.events.EventListViewModel
import com.revyakinpa.tinkofffintech.events.EventViewModel
import com.revyakinpa.tinkofffintech.lecture.LectureViewModel
import com.revyakinpa.tinkofffintech.performance.PerformanceViewModel
import com.revyakinpa.tinkofffintech.profile.ProfileViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(
        factory: ViewModelFactory
    ): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LectureViewModel::class)
    internal abstract fun lectureViewModel(viewModel: LectureViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PerformanceViewModel::class)
    internal abstract fun performanceViewModel(viewModel: PerformanceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EventViewModel::class)
    internal abstract fun eventViewModel(viewModel: EventViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EventListViewModel::class)
    internal abstract fun eventListViewModel(viewModel: EventListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    internal abstract fun profileViewModel(viewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CourseViewModel::class)
    internal abstract fun courseViewModel(viewModel: CourseViewModel): ViewModel
}