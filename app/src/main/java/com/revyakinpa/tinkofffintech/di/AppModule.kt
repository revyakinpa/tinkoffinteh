package com.revyakinpa.tinkofffintech.di

import android.app.Application
import androidx.room.Room
import com.revyakinpa.tinkofffintech.SharedPreferencesManager
import com.revyakinpa.tinkofffintech.lecture.LectureModel
import com.revyakinpa.tinkofffintech.lecture.LectureViewModel
import com.revyakinpa.tinkofffintech.storage.*
import dagger.Module
import dagger.Provides
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Singleton

@Module
class AppModule(val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideHomeworkDb(app: Application): HomeworkDb =
            Room.databaseBuilder(
                    app.applicationContext,
                    HomeworkDb::class.java,
                    "hw.db")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()

    @Provides
    @Singleton
    fun provideLectureDao(homeworkDb: HomeworkDb): LectureDao = homeworkDb.getLectureDao()

    @Provides
    @Singleton
    fun provideTaskDao(homeworkDb: HomeworkDb): TaskDao = homeworkDb.getTaskDao()

    @Provides
    @Singleton
    fun provideMarkDao(homeworkDb: HomeworkDb): MarkDao = homeworkDb.getMarkDao()

    @Provides
    @Singleton
    fun provideEventDao(homeworkDb: HomeworkDb): EventDao = homeworkDb.getEventDao()

    @Provides
    @Singleton
    fun provideProfileDao(homeworkDb: HomeworkDb): ProfileDao = homeworkDb.getProfileDao()


    @Provides
    @Singleton
    fun provideDateFormat(): SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH)


    @Provides
    @Singleton
    fun provideSharedPreferenceManager(app: Application): SharedPreferencesManager = SharedPreferencesManager.newManager(app.applicationContext)


    @Provides
    @Singleton
    fun provideLectureViewModel(app: Application, lectureModel: LectureModel): LectureViewModel = LectureViewModel(app, lectureModel)

}