package com.revyakinpa.tinkofffintech.di

import com.revyakinpa.tinkofffintech.NetManager
import com.revyakinpa.tinkofffintech.SharedPreferencesManager
import com.revyakinpa.tinkofffintech.course.CourseModel
import com.revyakinpa.tinkofffintech.events.EventListModel
import com.revyakinpa.tinkofffintech.events.EventModel
import com.revyakinpa.tinkofffintech.lecture.LectureModel
import com.revyakinpa.tinkofffintech.profile.ProfileModel
import com.revyakinpa.tinkofffintech.storage.HomeworkStorage
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ModelModules {

    @Provides
    @Singleton
    fun provideLectureModel(
            netManager: NetManager,
            sharedPreferencesManager: SharedPreferencesManager,
            homeworkStorage: HomeworkStorage,
            fintechService: FintechService
    ): LectureModel {
        return LectureModel(netManager, sharedPreferencesManager, homeworkStorage, fintechService)
    }

    @Provides
    @Singleton
    fun provideEventModel(
            netManager: NetManager,
            sharedPreferencesManager: SharedPreferencesManager,
            homeworkStorage: HomeworkStorage,
            fintechService: FintechService
    ): EventModel {
        return EventModel(netManager, sharedPreferencesManager, homeworkStorage, fintechService)
    }

    @Provides
    @Singleton
    fun provideEventListModel(
            netManager: NetManager,
            sharedPreferencesManager: SharedPreferencesManager,
            homeworkStorage: HomeworkStorage,
            fintechService: FintechService
    ): EventListModel {
        return EventListModel(netManager, sharedPreferencesManager, homeworkStorage, fintechService)
    }

    @Provides
    @Singleton
    fun provideProfileModel(
            netManager: NetManager,
            sharedPreferencesManager: SharedPreferencesManager,
            homeworkStorage: HomeworkStorage,
            fintechService: FintechService
    ): ProfileModel {
        return ProfileModel(netManager, sharedPreferencesManager, homeworkStorage, fintechService)
    }

    @Provides
    @Singleton
    fun provideCourseModel(
            netManager: NetManager,
            sharedPreferencesManager: SharedPreferencesManager,
            homeworkStorage: HomeworkStorage,
            fintechService: FintechService
    ): CourseModel {
        return CourseModel(netManager, sharedPreferencesManager, homeworkStorage, fintechService)
    }
}