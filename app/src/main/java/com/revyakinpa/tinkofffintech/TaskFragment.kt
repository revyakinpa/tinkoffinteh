package com.revyakinpa.tinkofffintech

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.revyakinpa.tinkofffintech.di.AppModule
import com.revyakinpa.tinkofffintech.di.DaggerAppComponent
import com.revyakinpa.tinkofffintech.di.ModelModules
import com.revyakinpa.tinkofffintech.di.NetModule
import com.revyakinpa.tinkofffintech.storage.HomeworkStorage
import com.revyakinpa.tinkofffintech.storage.Task
import kotlinx.android.synthetic.main.fragment_task.*
import javax.inject.Inject

class TaskFragment : Fragment() {

    @Inject
    lateinit var homeworkStorage: HomeworkStorage

    private lateinit var tasks: List<Task>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_task, container, false)
    }

    companion object {
        fun newInstance(lectureId: Int): TaskFragment {
            return TaskFragment().apply {
                arguments = Bundle().apply {
                    putInt("lectureId", lectureId)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        DaggerAppComponent.builder()
                .appModule(AppModule(requireActivity().application))
                .netModule(NetModule())
                .modelModules(ModelModules())
                .build().inject(this)

        tasks = homeworkStorage.getTasksByLecture(arguments!!.getInt("lectureId"))
        task_recycler.layoutManager = LinearLayoutManager(requireContext())
        task_recycler.adapter = TaskAdapter(tasks)
    }

}