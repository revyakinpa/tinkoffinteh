package com.revyakinpa.tinkofffintech.performance


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.revyakinpa.tinkofffintech.R

class PerformanceActivity : AppCompatActivity() {

    private val FRAGMENT_TAG = "tag"

    private lateinit var fragment: PerformanceFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_performance)

        if (savedInstanceState == null) {
            fragment = PerformanceFragment.newInstance()
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, FRAGMENT_TAG)
                    .commitNow()
        } else {
            fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG) as PerformanceFragment
        }

    }




}
