package com.revyakinpa.tinkofffintech.performance

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.SearchView
import android.view.*
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.revyakinpa.tinkofffintech.*
import kotlinx.android.synthetic.main.fragment_performance.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.revyakinpa.tinkofffintech.storage.Mark
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class PerformanceFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: PerformanceViewModel

    private lateinit var performanceAdapter: ContactAdapter
    private var layoutType = 0

    private var searchView: SearchView? = null
    private var SORT_TYPE_ABC = 1
    private var SORT_TYPE_POINT = 2
    private var sortType = 0

    private var marks: List<Mark> = listOf()

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PerformanceViewModel::class.java)

        contacts_recycler.layoutManager = LinearLayoutManager(requireContext())
        performanceAdapter = ContactAdapter(marks, layoutType)
        contacts_recycler.adapter = performanceAdapter

        viewModel.getStudents()


        viewModel.marksState.observe(this, Observer<PerformanceResult> { either ->

            when (either) {
                is Left -> {
                    either.value.let {
                        marks = it
                        performanceAdapter.updateList(it)
                    }
                }
                is Right -> {
                    Toast.makeText(requireContext(), either.value, Toast.LENGTH_SHORT).show()
                }
            }
        })

        viewModel.isLoading.observe(this, Observer {
            if (it) {
                if (!swiperefresh.isRefreshing) {
                    contacts_recycler.visibility = View.GONE
                    progressBar.visibility = View.VISIBLE
                }
            } else {
                contacts_recycler.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                swiperefresh.isRefreshing = false
            }
        })

        swiperefresh.setOnRefreshListener {
            viewModel.updateStudents()
        }

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_performance, container, false)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.performance, menu)

        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                performanceAdapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                performanceAdapter.filter.filter(query)
                return false
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.performance_change -> {
                changeLayoutManager()
                true
            }
            R.id.performance_sort_by_name -> {
                sortType = SORT_TYPE_ABC
                marks = sortByType()
                performanceAdapter.updateList(marks)
                performanceAdapter.notifyDataSetChanged()
                true
            }
            R.id.performance_sort_by_mark -> {
                sortType = SORT_TYPE_POINT
                marks = sortByType()
                performanceAdapter.updateList(marks)
                performanceAdapter.notifyDataSetChanged()
                true
            }
            R.id.action_search -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun changeLayoutManager() {
        if (contacts_recycler.layoutManager is GridLayoutManager) {
            contacts_recycler.layoutManager = LinearLayoutManager(requireContext())
        } else {
            contacts_recycler.layoutManager = GridLayoutManager(requireContext(), 3)
        }
        performanceAdapter.changeLayoutType()
        performanceAdapter.notifyDataSetChanged()

    }

    private fun sortByType(): List<Mark> {
        return when (sortType) {
            (SORT_TYPE_ABC) -> marks.sortedWith(compareBy { it.student })
            (SORT_TYPE_POINT) -> marks.sortedWith(compareByDescending<Mark> { it.mark }.thenBy { it.student })
            else -> marks
        }
    }


    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    companion object {
        fun newInstance(): PerformanceFragment {
            return PerformanceFragment()
        }
    }

}