package com.revyakinpa.tinkofffintech.performance

import com.revyakinpa.tinkofffintech.StudentGroup
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header

interface PerformanceService {
    @GET("course/android_spring_2019/grades")
    fun getStudentsGroups(@Header("Cookie") cookie: String): Call<List<StudentGroup>>
}