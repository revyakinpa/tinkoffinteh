package com.revyakinpa.tinkofffintech.performance

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.revyakinpa.tinkofffintech.*
import com.revyakinpa.tinkofffintech.di.FintechService
import com.revyakinpa.tinkofffintech.storage.HomeworkStorage
import com.revyakinpa.tinkofffintech.storage.Mark
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class PerformanceModel @Inject constructor(
        private val netManager: NetManager,
        private val sharedPreferencesManager: SharedPreferencesManager,
        private val homeworkStorage: HomeworkStorage,
        private val performanceService: FintechService
) {


    private val mutableMarksState = MutableLiveData<PerformanceResult>()
    val marksState: LiveData<PerformanceResult> = mutableMarksState

    val isLoading = MutableLiveData<Boolean>()


    fun getStudents() {
        isLoading.value = true
        if (homeworkStorage.getMarksCount() == 0 || sharedPreferencesManager.isDataOld()) {
            updateStudents()
        } else {
            mutableMarksState.value = Left(homeworkStorage.getMarks())
            isLoading.value = false
        }
    }

    @SuppressLint("CheckResult")
    fun getStudentsRX(): Observable<List<Mark>> {
        val cookie = sharedPreferencesManager.getCookie()

        /**
         * Это я пытался два запроса сделать, чтобы потом свое имя поменять на "Вы",
         * не получилось, а времени уже не оставалось
         */
//        return Single.zip(
//                fromCallable { performanceService.getStudentsGroupsRX(cookie) },
//                fromCallable { performanceService.getProfileRX(cookie) },
//                BiFunction { students: Observable<List<StudentGroup>>, profile: Single<ResponseUser> -> Pair(students, profile) }
//        ).map { pair ->
//            pair.first.flatMap { Observable.fromIterable(it) }
//                    .map {
//                        homeworkStorage.saveMarks(it.students)
//                        homeworkStorage.getMarks()
//                    }
//                    .flatMap { Observable.fromIterable(it) }
//                    .filter { it.mark > 20 }
//                    .flatMap { Observable.just(it) }
//                    .toList()
//                    .toObservable()
//        }.flatMap { Single.fromObservable(it) }


        return performanceService.getStudentsGroupsRX(cookie)
                .flatMap {
                    Observable.fromIterable(it)
                }
                .map {
                    homeworkStorage.saveMarks(it.students)
                    homeworkStorage.getMarks()
                }
                .flatMap { Observable.fromIterable(it) }
                .filter { it.mark > 20 }
//                .map {
//                    if (it.student == "Ревякин Петр") it.student = "Вы"
//                    it
//                }
                .flatMap { Observable.just(it) }
                .toList()
                .toObservable()


    }

    fun updateStudents() {
        isLoading.value = true
        if (sharedPreferencesManager.isDataOld()) {
            netManager.isConnectedToInternet?.let {
                if (it) {
                    performanceService
                            .getStudentsGroups(sharedPreferencesManager.getCookie(), sharedPreferencesManager.getCourseUrl())
                            .enqueue(object : Callback<List<StudentGroup>> {
                                override fun onFailure(call: Call<List<StudentGroup>>, t: Throwable) {
                                    mutableMarksState.value = Right("Ошибка запроса")
                                    isLoading.value = false
                                }

                                override fun onResponse(
                                        call: Call<List<StudentGroup>>,
                                        response: Response<List<StudentGroup>>
                                ) {
                                    if (response.isSuccessful) {
                                        homeworkStorage.clearMarks()
                                        homeworkStorage.saveMarks(response.body()!![1].students)
                                        mutableMarksState.value = Left(homeworkStorage.getMarks())
                                        sharedPreferencesManager.setMarkSavedTime()
                                    } else {
                                        mutableMarksState.value = Right("Ошибка ответа")
                                    }
                                    isLoading.value = false
                                }

                            })
                } else {
                    mutableMarksState.value = Right("Ты забыл включить интернет")
                    isLoading.value = false
                }
            }
        } else {
            mutableMarksState.value = Left(homeworkStorage.getMarks())
            isLoading.value = false
        }
    }
}

typealias PerformanceResult = Either<List<Mark>, String>