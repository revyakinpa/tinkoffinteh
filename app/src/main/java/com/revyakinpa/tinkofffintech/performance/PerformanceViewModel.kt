package com.revyakinpa.tinkofffintech.performance

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.revyakinpa.tinkofffintech.storage.Mark
import io.reactivex.Observable
import javax.inject.Inject

class PerformanceViewModel @Inject constructor(
        app: Application,
        private val model: PerformanceModel
) : AndroidViewModel(app) {

    val marksState = model.marksState
    val isLoading = model.isLoading

    fun getStudents() {
        model.getStudents()
    }

    fun updateStudents() {
        model.updateStudents()
    }

    fun getStudentsRX(): Observable<List<Mark>> {
        return model.getStudentsRX()
    }

}