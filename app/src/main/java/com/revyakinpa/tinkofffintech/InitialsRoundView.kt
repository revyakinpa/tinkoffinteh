package com.revyakinpa.tinkofffintech

import android.content.Context
import android.graphics.PorterDuff
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.initials_round_view.view.*


class InitialsRoundView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var roundBack: FrameLayout
    private var initials: TextView

    init {
        LayoutInflater.from(context)
                .inflate(R.layout.initials_round_view, this, true)

        roundBack = round_back
        initials = initials_text

        val ta = context.obtainStyledAttributes(attrs, R.styleable.InitialsRoundView)
        try {
            val initialsText = ta.getString(R.styleable.InitialsRoundView_initials)
            val roundColor = ta.getColor(R.styleable.InitialsRoundView_roundColor, 0)
            setInitials(initialsText)
            setRoundColor(roundColor)
        } finally {
            ta.recycle()
        }
    }

    fun setInitials(value: String) {
        initials.text = value
    }

    fun setRoundColor(value: Int) {
        roundBack.background.setColorFilter(value, PorterDuff.Mode.SRC_IN)
    }
}