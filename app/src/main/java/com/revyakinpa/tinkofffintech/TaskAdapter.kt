package com.revyakinpa.tinkofffintech

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.revyakinpa.tinkofffintech.storage.Task

class TaskAdapter(var tasks: List<Task>) :
        RecyclerView.Adapter<TaskAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_task, parent, false))

    }

    override fun onBindViewHolder(holder: ViewHolder, index: Int) = holder.bind(tasks[index])

    override fun getItemCount() = tasks.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title: TextView = view.findViewById(R.id.title)
        private val status: TextView = view.findViewById(R.id.status)
        private val mark: TextView = view.findViewById(R.id.mark)
        private val deadlineDate: TextView = view.findViewById(R.id.deadline_date)

        fun bind(task: Task) {
            title.text = task.title
            status.text = task.status
            mark.text = "${task.mark}/${task.max_score}"
            if (task.deadline_date != null) {
                deadlineDate.visibility = View.VISIBLE
                deadlineDate.text = "Сдать до ${task.deadline_date}"
            }
        }
    }
}