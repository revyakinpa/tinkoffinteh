package com.revyakinpa.tinkofffintech

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class CourseReadyFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_course_ready, container, false)
    }


    companion object {
        fun newInstance(): CourseReadyFragment {
            return CourseReadyFragment()
        }
    }
}