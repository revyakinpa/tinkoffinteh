package com.revyakinpa.tinkofffintech.lecture

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import javax.inject.Inject

class LectureViewModel @Inject constructor(
        app: Application,
        private val model: LectureModel
) : AndroidViewModel(app) {

    val lecturesState = model.lecturesState
    val isLoading = model.isLoading

    fun getLectures() {
        model.getLectures()
    }

    fun loadLectures() {
        model.loadLectures()
    }

}