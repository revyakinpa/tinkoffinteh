package com.revyakinpa.tinkofffintech.lecture

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.revyakinpa.tinkofffintech.*
import com.revyakinpa.tinkofffintech.di.FintechService
import com.revyakinpa.tinkofffintech.storage.HomeworkStorage
import com.revyakinpa.tinkofffintech.storage.Lecture
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class LectureModel @Inject constructor(
        private val netManager: NetManager,
        private val sharedPreferencesManager: SharedPreferencesManager,
        private val homeworkStorage: HomeworkStorage,
        private val lectureService: FintechService
) {


    private val mutableLecturesState = MutableLiveData<LectureResult>()
    val lecturesState: LiveData<LectureResult> = mutableLecturesState

    val isLoading = MutableLiveData<Boolean>()


    fun getLectures() {
        isLoading.value = true
        if (homeworkStorage.getLectureCount() == 0) {
            loadLectures()
        } else {
            mutableLecturesState.value = Left(homeworkStorage.getLectures())
            isLoading.value = false
        }
    }

    fun loadLectures() {
        netManager.isConnectedToInternet?.let {
            if (it) {
                lectureService
                        .getHomeworks((sharedPreferencesManager.getCookie()))
                        .enqueue(object : Callback<Homeworks> {
                            override fun onFailure(call: Call<Homeworks>, t: Throwable) {
                                mutableLecturesState.value = Right("Ошибка запроса")
                                isLoading.value = false
                            }

                            override fun onResponse(call: Call<Homeworks>, response: Response<Homeworks>) {
                                if (response.isSuccessful) {
                                    homeworkStorage.clearTables()
                                    homeworkStorage.saveHomeworks(response.body()!!.homeworks)
                                    mutableLecturesState.value = Left(homeworkStorage.getLectures())
                                } else {
                                    mutableLecturesState.value = Right("Ошибка ответа")
                                }
                                isLoading.value = false
                            }
                        })

            } else {
                isLoading.value = false
                mutableLecturesState.value = Right("Ты не включил интернет")
            }
        }
    }

}

/**
 * Если у нас есть Лекции, то передаем их (Left), иначе передаем сообщение ошибки (Right)
 */
    typealias LectureResult = Either<List<Lecture>, String>