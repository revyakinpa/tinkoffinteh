package com.revyakinpa.tinkofffintech.lecture

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.revyakinpa.tinkofffintech.*
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_lecture.*
import javax.inject.Inject


class LectureFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: LectureViewModel

    private lateinit var lectureAdapter: LectureAdapter

    private var listener: LectureListener? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LectureViewModel::class.java)

        lectures_recycler.layoutManager = LinearLayoutManager(requireContext())
        val lectureInterface = object : LectureAdapter.LectureAdapterInterface {
            override fun onItemCliked(lectureId: Int) {
                listener!!.addTaskFragment(TaskFragment.newInstance(lectureId))
            }
        }
        lectureAdapter = LectureAdapter(listOf(), lectureInterface)
        lectures_recycler.adapter = lectureAdapter

        viewModel.getLectures()


        viewModel.lecturesState.observe(this, Observer<LectureResult> { either ->

            when (either) {
                is Left -> {
                    either.value.let {
                        lectureAdapter.lectures = it
                    }
                }
                is Right -> {
                    Toast.makeText(requireContext(), either.value, Toast.LENGTH_SHORT).show()
                }
            }

        })

        viewModel.isLoading.observe(this, Observer {
            if (it) {
                if (!refresh.isRefreshing) {
                    lectures_recycler.visibility = View.GONE
                    progressBar.visibility = View.VISIBLE
                }
            } else {
                lectures_recycler.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                refresh.isRefreshing = false
            }
        })

        refresh.setOnRefreshListener {
            viewModel.loadLectures()
        }
    }


    interface LectureListener {
        fun addTaskFragment(fragment: Fragment)
    }


    companion object {
        fun newInstance(): LectureFragment {
            return LectureFragment()
        }
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        if (context is LectureListener) {
            listener = context
        } else {
            throw ClassCastException("$context is not LectureListener")
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_lecture, container, false)
    }


}
