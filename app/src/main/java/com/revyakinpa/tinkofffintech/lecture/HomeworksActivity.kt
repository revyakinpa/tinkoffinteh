package com.revyakinpa.tinkofffintech.lecture


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.revyakinpa.tinkofffintech.R


class HomeworksActivity : AppCompatActivity(), LectureFragment.LectureListener {


    private val FRAGMENT_LEC_TAG = "lecture"
    private val FRAGMENT_TASK_TAG = "task"

    private lateinit var fragment: LectureFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_homeworks)

        if (savedInstanceState == null) {
            fragment = LectureFragment.newInstance()
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment, fragment, FRAGMENT_LEC_TAG)
                .commitNow()
        } else {
            fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_LEC_TAG) as LectureFragment
        }

    }


    override fun addTaskFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment, fragment, FRAGMENT_TASK_TAG)
            .addToBackStack(FRAGMENT_TASK_TAG)
            .commit()
    }
}
