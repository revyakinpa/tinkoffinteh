package com.revyakinpa.tinkofffintech.lecture

import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.revyakinpa.tinkofffintech.storage.Lecture
import com.revyakinpa.tinkofffintech.R


class LectureAdapter(var lectures: List<Lecture>, var lectureInterface: LectureAdapterInterface) :
    RecyclerView.Adapter<LectureAdapter.ViewHolder>() {



    interface LectureAdapterInterface {
        fun onItemCliked(lectureId: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_lecture, parent, false))

    }

    override fun onBindViewHolder(holder: ViewHolder, index: Int) = holder.bind(lectures[index], lectureInterface)

    override fun getItemCount() = lectures.size



    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title: TextView = view.findViewById(R.id.lecture_title)
        private val card: CardView = view.findViewById(R.id.lecture_card)

        fun bind(lecture: Lecture, lectureInterface: LectureAdapterInterface) {
            title.text = lecture.title

            card.setOnClickListener {
                lectureInterface.onItemCliked(lecture.lectureId)
            }

        }
    }
}