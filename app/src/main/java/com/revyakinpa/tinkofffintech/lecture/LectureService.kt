package com.revyakinpa.tinkofffintech.lecture

import com.revyakinpa.tinkofffintech.Homeworks
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header

interface LectureService {
    @GET("course/android_spring_2019/homeworks")
    fun getHomeworks(@Header("Cookie") cookie: String): Call<Homeworks>
}