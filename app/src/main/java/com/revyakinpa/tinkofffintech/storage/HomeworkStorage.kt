package com.revyakinpa.tinkofffintech.storage


import android.os.Parcelable
import androidx.room.*
import com.revyakinpa.tinkofffintech.Homework
import com.revyakinpa.tinkofffintech.Students
import com.revyakinpa.tinkofffintech.events.EventType
import com.revyakinpa.tinkofffintech.events.Events
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@Database(entities = [
    Lecture::class,
    Task::class,
    Mark::class,
    Event::class,
    Profile::class
], version = 5)
abstract class HomeworkDb : RoomDatabase() {

    abstract fun getLectureDao(): LectureDao

    abstract fun getTaskDao(): TaskDao

    abstract fun getMarkDao(): MarkDao

    abstract fun getEventDao(): EventDao

    abstract fun getProfileDao(): ProfileDao

}


@Dao
interface LectureDao {

    @Insert
    fun saveLecture(lecture: Lecture)


    @Query("SELECT COUNT(*) FROM lecture")
    fun getCount(): Int

    @Query("SELECT * FROM lecture")
    fun getLectures(): List<Lecture>

    @Query("DELETE FROM lecture")
    fun clearLecture()
}

@Dao
interface TaskDao {

    @Insert
    fun saveTask(task: Task)

    @Query("SELECT * FROM task WHERE lectureId = :lectureId")
    fun getTasksByLecture(lectureId: Int): List<Task>

    @Query("DELETE FROM task")
    fun clearTask()

    @Query("SELECT COUNT(*) FROM task WHERE taskType = :taskType")
    fun getCountOfType(taskType: String): Int

    @Query("SELECT COUNT(*) FROM task WHERE taskType = :taskType and status = 'Зачтено'")
    fun getCountOfTypeAccepted(taskType: String): Int
}

@Dao
interface MarkDao {
    @Insert
    fun saveGrade(mark: Mark)

    @Query("SELECT * FROM mark")
    fun getMarks(): List<Mark>

    @Query("SELECT COUNT(*) FROM mark")
    fun getCount(): Int

    @Query("DELETE FROM mark")
    fun clearMark()

    @Query("SELECT * FROM mark ORDER BY mark desc limit 10")
    fun getMarksTop10(): List<Mark>

    @Query("SELECT mark FROM mark WHERE student = :student")
    fun getMarkByName(student: String): List<Double>

    @Query("SELECT student FROM mark ORDER BY mark desc")
    fun getMarksTopNames(): List<String>
}

@Dao
interface EventDao {

    @Insert
    fun saveEvent(event: Event)

    @Query("SELECT COUNT(*) FROM event")
    fun getCount(): Int

    @Query("SELECT * FROM event")
    fun getEvents(): List<Event>

    @Query("SELECT * FROM event WHERE type = 'active'")
    fun getEventsActive(): List<Event>

    @Query("SELECT * FROM event WHERE type = 'archive'")
    fun getEventsArchive(): List<Event>

    @Query("DELETE FROM event")
    fun clearEvent()
}

@Dao
interface ProfileDao{
    @Insert
    fun saveProfile(profile: Profile)

    @Query("DELETE FROM profile")
    fun deleteProfile()

    @Query("SELECT * FROM profile")
    fun getProfile(): List<Profile>
}


@Entity
data class Lecture(@PrimaryKey val id: Long? = null, val lectureId: Int, val title: String)

@Entity
data class Task(
        @PrimaryKey val id: Long? = null, val lectureId: Int,
        val title: String,
        val status: String,
        val mark: Double,
        val max_score: Double,
        val deadline_date: String? = null,
        val taskType: String
)

@Entity
data class Mark(@PrimaryKey val id: Long? = null, val mark: Double, var student: String)

@Parcelize
@Entity
data class Event(
    @PrimaryKey val id: Long? = null,
    val type: String,
    val title: String,
    val dateStart: String,
    val dateEnd: String,
    val eventTypeName: String,
    val eventTypeColor: String,
    val place: String,
    val url: String,
    val urlExternal: Boolean
) : Parcelable

@Entity
data class Profile(
        @PrimaryKey val id: Long? = null,
        val birthday: String?,
        val email: String,
        val firstName: String,
        val lastName: String,
        val middleName: String,
        val phoneMobile: String?,
        val tShirtSize: String?,
        val region: String?,
        val university: String?,
        val faculty: String?,
        val avatar: String?
)

class HomeworkStorage @Inject constructor (
        val database: HomeworkDb,
        val dateFormat: SimpleDateFormat
) {

    fun getLectureCount(): Int {
        return database.getLectureDao().getCount()
    }

    fun saveHomeworks(homeworks: List<Homework>) {
        for (homework in homeworks) {
            val newLecture =
                Lecture(lectureId = homework.id, title = homework.title)
            database.getLectureDao().saveLecture(newLecture)
            for (task in homework.tasks) {
                var deadlineDate = task.task.deadlineDate
                if (deadlineDate != null) {
                    val d = dateFormat.parse(deadlineDate.replace("000Z", "+0000"))
                    deadlineDate = SimpleDateFormat("dd MMM yyyy|HH:mm", Locale.getDefault()).format(d).toString()
                }
                val newTask = Task(
                    lectureId = homework.id,
                    title = task.task.title,
                    status = renameStatus(task.status),
                    mark = task.mark,
                    max_score = task.task.maxScore,
                    deadline_date = deadlineDate,
                    taskType = task.task.taskType
                )
                database.getTaskDao().saveTask(newTask)
            }
        }
    }

    fun getLectures(): List<Lecture> {
        return database.getLectureDao().getLectures()
    }

    fun getTasksByLecture(lectureId: Int): List<Task> {
        return database.getTaskDao().getTasksByLecture(lectureId)
    }

    fun clearTables() {
        database.getTaskDao().clearTask()
        database.getLectureDao().clearLecture()
    }

    private fun renameStatus(s: String): String {
        return when (s) {
            "accepted" -> "Зачтено"
            "new" -> "Новый"
            else -> "На проверке"
            /*Я просто забыл что пишется при статусе "На проверке",
            а в момент когда писал этот код, у меня не было такси с таким статусом
             */
        }
    }

    /**
     * MARK
     */

    fun saveMarks(students: List<Students>) {
        for (student in students) {
            val name = student.name
            val lastIndex = student.grades.lastIndex
            val mark = student.grades[lastIndex].mark
            database.getMarkDao().saveGrade(Mark(mark = mark, student = name))
        }
    }

    fun getMarks(): List<Mark> {
        return database.getMarkDao().getMarks()
    }

    fun getMarksCount(): Int {
        return database.getMarkDao().getCount()
    }

    fun clearMarks() {
        database.getMarkDao().clearMark()
    }

    fun getMarksTop10(): List<Mark> {
        return database.getMarkDao().getMarksTop10()
    }

    /**
     * EVENTS
     */

    fun getEventCount(): Int {
        return database.getEventDao().getCount()
    }

    fun getEvents(): List<List<Event>> {
        return listOf(
            database.getEventDao().getEventsActive(),
            database.getEventDao().getEventsArchive()
        )
    }

    fun getEventsActive(): List<Event> {
        return database.getEventDao().getEventsActive()
    }

    fun getEventsArchive(): List<Event> {
        return database.getEventDao().getEventsArchive()
    }

    fun clearEvents() {
        database.getEventDao().clearEvent()
    }

    fun saveEvents(events: Events) {
        for (ev in events.active) {
            var evType = ev.eventType
            if (evType == null) {
                evType = EventType(name = "", color = "green")
            }
            database.getEventDao().saveEvent(
                Event(
                    type = "active",
                    title = ev.title,
                    dateStart = ev.dateStart,
                    dateEnd = ev.dateEnd,
                    eventTypeName = evType.name,
                    eventTypeColor = evType.color,
                    place = ev.place,
                    url = ev.url,
                    urlExternal = ev.urlExternal
                )
            )
        }

        for (ev in events.archive) {
            var evType = ev.eventType
            if (evType == null) {
                evType = EventType(name = "", color = "green")
            }
            database.getEventDao().saveEvent(
                Event(
                    type = "archive",
                    title = ev.title,
                    dateStart = ev.dateStart,
                    dateEnd = ev.dateEnd,
                    eventTypeName = evType.name,
                    eventTypeColor = evType.color,
                    place = ev.place,
                    url = ev.url,
                    urlExternal = ev.urlExternal
                )
            )
        }
    }

    /**
     *  PROFILE
     */

    fun reSaveProfile(profile: com.revyakinpa.tinkofffintech.profile.Profile) {
        database.getProfileDao().deleteProfile()
        database.getProfileDao().saveProfile(
                Profile(
                        birthday = profile.birthday,
                        email = profile.email,
                        firstName = profile.firstName,
                        lastName = profile.lastName,
                        middleName = profile.middleName,
                        phoneMobile = profile.phoneMobile,
                        tShirtSize = profile.tShirtSize,
                        region = profile.region,
                        university = profile.university,
                        faculty = profile.faculty,
                        avatar = profile.avatar
                )
        )
    }

    fun isProfile(): Boolean {
        return database.getProfileDao().getProfile().isNotEmpty()
    }

    fun getProfileName(): String {
        val profile = database.getProfileDao().getProfile()[0]
        return "${profile.lastName} ${profile.firstName}"
    }


    /**
     * RATING
     */

    fun getProfileGrade(name: String): Double {
        return database.getMarkDao().getMarkByName(name)[0]
    }

    fun getNamePositionInRating(name:String): Int {
        val students = database.getMarkDao().getMarksTopNames()
        return students.indexOf(name) + 1
    }

    fun getCountOfTypeAccepted(taskType: String): Int {
        return database.getTaskDao().getCountOfTypeAccepted(taskType)
    }

    fun getCountOfType(taskType: String): Int {
        return database.getTaskDao().getCountOfType(taskType)
    }
}


