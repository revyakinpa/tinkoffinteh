package com.revyakinpa.tinkofffintech

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.user_performance_view.view.*


class UserPerformanceView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var userName: TextView
    private var userBage: TextView
    private var userImg: FrameLayout

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.user_performance_view, this, true)

        userName = user_name
        userBage = user_score
        userImg = user_img

        val ta = context.obtainStyledAttributes(attrs, R.styleable.UserPerformanceView)
        try {
            val bageText = ta.getInteger(R.styleable.UserPerformanceView_badgeText, 0)
            setBageValue(bageText)
        } finally {
            ta.recycle()
        }

    }

    fun setBageValue(value: Int) {
        if (value != 0) {
            userBage.text = value.toString()
            userBage.visibility = View.VISIBLE
        } else {
            userBage.visibility = View.GONE
        }
    }

    fun setColorImg(color: Int, pd: PorterDuff.Mode) {
        userImg.background.setColorFilter(color, pd)
    }

    fun setUserName (name: String) {
        userName.text = name
        if (name == "Вы") {
            makeSelf()
        }
    }

    private fun makeSelf () {
        userName.typeface = Typeface.DEFAULT_BOLD
        userImg.setBackgroundResource(R.drawable.user_ico_shape_self)
    }

}