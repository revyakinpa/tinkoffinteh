package com.revyakinpa.tinkofffintech

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import com.revyakinpa.tinkofffintech.course.CourseFragment
import com.revyakinpa.tinkofffintech.events.EventFragment
import com.revyakinpa.tinkofffintech.profile.ProfileFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(),
        ProfileEditDialogFragment.EditDialogListener {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_event -> {
                setFragment(EventFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_course -> {
                setFragment(CourseFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                setFragment(ProfileFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun setFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment, fragment)
                .commitNow()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            setFragment(CourseFragment.newInstance())
            navigation.selectedItemId = R.id.navigation_course
        }

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

    }


    override fun closeFragment() {
        supportFragmentManager.popBackStackImmediate()
    }



}
