package com.revyakinpa.tinkofffintech

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.annotation.NonNull
import androidx.fragment.app.DialogFragment

class ProfileEditDialogFragment : DialogFragment() {

    var listener : EditDialogListener? = null

    interface EditDialogListener {
        fun closeFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is EditDialogListener) {
            listener = context
        } else {
            throw ClassCastException("$context is not EditDialogListener")
        }

    }

    @NonNull
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val message = arguments?.getString("message")
        val builder = AlertDialog.Builder(requireContext())

        if (message != null) {
            builder.setMessage(message)
        }

        builder.setNegativeButton(getString(R.string.dialog_negative_button)) { dialog, _ ->
            listener?.closeFragment() // Закрываем фрагмент Редактирования
        }

        builder.setPositiveButton(getString(R.string.dialog_positive_button)) { dialog, _ ->
            dialog.dismiss()
        }
        return builder.create()
    }
}