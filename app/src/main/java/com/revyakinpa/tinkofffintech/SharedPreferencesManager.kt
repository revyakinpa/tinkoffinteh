package com.revyakinpa.tinkofffintech

import android.content.Context
import android.content.SharedPreferences
import com.revyakinpa.tinkofffintech.course.Course
import java.util.*

class SharedPreferencesManager {

    companion object {
        lateinit var sharedPreferences: SharedPreferences

        fun newManager(context: Context): SharedPreferencesManager {
            sharedPreferences = context.getSharedPreferences(R.string.APP_PREFERENCES.toString(), Context.MODE_PRIVATE)
            return SharedPreferencesManager()
        }
    }

    fun getCookie(): String = sharedPreferences.getString("cookie", "")

    fun saveCookie(cookie: String, expires: String) {
        sharedPreferences.edit()
                .putString("cookie", cookie)
                .putString("expires", expires)
                .apply()
    }

    fun setMarkSavedTime() {
        sharedPreferences.edit().putLong("markSaveTime", Date().time).apply()
    }

    fun isDataOld(): Boolean {
        val current = Date().time
        val prev = sharedPreferences.getLong("markSaveTime", current - 10001)
        return current - prev > 10000
    }

    fun containsCookie(): Boolean = sharedPreferences.contains("cookie")


    fun getExpires(): String = sharedPreferences.getString("expires", "")

    fun revomeCookies() {
        sharedPreferences.edit().apply {
            remove("cookie")
            remove("expires")
        }.apply()
    }


    fun saveCourseInfo(course: Course) {
        sharedPreferences.edit().apply{
            putString("courseName", course.title)
            putString("courseUrl", course.url)
        }.apply()
    }

    fun getCourseUrl(): String {
        return sharedPreferences.getString("courseUrl", "")
    }
}
