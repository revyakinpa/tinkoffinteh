package com.revyakinpa.tinkofffintech

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class ResponseUser(val user: User, val status: String)

@Parcelize
data class User(
        var middle_name: String,
        var first_name: String,
        var last_name: String,
        var avatar: String?
) : Parcelable
