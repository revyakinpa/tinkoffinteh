package com.revyakinpa.tinkofffintech.course.rating

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.revyakinpa.tinkofffintech.Left
import com.revyakinpa.tinkofffintech.R
import com.revyakinpa.tinkofffintech.course.CourseViewModel
import com.revyakinpa.tinkofffintech.lecture.HomeworksActivity
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_course_rating.*
import javax.inject.Inject

class CourseRatingFragment : DaggerFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: CourseViewModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CourseViewModel::class.java)

//        viewModel.getRating()

        viewModel.ratingState.observe(this, Observer {
            when (it) {
                is Left -> {
                    val rating = it.value
                    user_rating.text = rating[0]
                    user_count_lec.text = "Всего лекций: ${rating[1]}"
                    user_position.text = rating[2]
                    user_test_accept.text = rating[3]
                    user_hw_accept.text = rating[4]
                }
            }
        })

        header.setOnClickListener {
            val intent = Intent(context, HomeworksActivity::class.java)
            context!!.startActivity(intent)
        }
    }


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_course_rating, container, false)
    }


    companion object {
        fun newInstance(): CourseRatingFragment {
            return CourseRatingFragment()
        }
    }
}