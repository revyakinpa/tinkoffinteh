package com.revyakinpa.tinkofffintech.course.performance

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.revyakinpa.tinkofffintech.Left
import com.revyakinpa.tinkofffintech.R
import com.revyakinpa.tinkofffintech.Right
import com.revyakinpa.tinkofffintech.UserPerformanceView
import com.revyakinpa.tinkofffintech.course.CourseViewModel
import com.revyakinpa.tinkofffintech.performance.PerformanceActivity
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_course_performance.*
import javax.inject.Inject

class CoursePerformanceFragment : DaggerFragment() {

    companion object {
        fun newInstance(): CoursePerformanceFragment {
            return CoursePerformanceFragment()
        }
    }

    private var users: Array<UserPerformanceView?> = arrayOf()

    private var uBages: ArrayList<Int> = arrayListOf()


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: CourseViewModel

    private lateinit var coursePerformanceAdapter: CoursePerformanceAdapter

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_course_performance, container, false)
    }


    @SuppressLint("NewApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CourseViewModel::class.java)

        course_performance_recycler.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        coursePerformanceAdapter = CoursePerformanceAdapter(listOf())
        course_performance_recycler.adapter = coursePerformanceAdapter

//        viewModel.getMarksTop10()

        viewModel.marksTop10State.observe(this, Observer {
            when(it) {
                is Left -> {
                    println(it.value)
//                    coursePerformanceAdapter.marks = it.value
                    coursePerformanceAdapter.updateList(it.value)
//                    viewModel.getRating()
                }
                is Right -> {
                    Toast.makeText(requireContext(), it.value, Toast.LENGTH_SHORT).show()
                }
            }
        })


        btn_details.setOnClickListener {
            val intent = Intent(context, PerformanceActivity::class.java)
            context!!.startActivity(intent)
        }
    }



    fun setBages(bages: ArrayList<Int>) {
        uBages = bages
        val right = uBages.size - 1
        for (i in 0..right) {
            val user = users[i]
            user!!.setBageValue(uBages[i])
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putIntegerArrayList("ubages", uBages)
    }


}
