package com.revyakinpa.tinkofffintech.course

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.revyakinpa.tinkofffintech.*
import com.revyakinpa.tinkofffintech.di.FintechService
import com.revyakinpa.tinkofffintech.profile.ResponseUser
import com.revyakinpa.tinkofffintech.storage.HomeworkStorage
import com.revyakinpa.tinkofffintech.storage.Mark
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigDecimal
import javax.inject.Inject

class CourseModel @Inject constructor(
        private val netManager: NetManager,
        private val sharedPreferencesManager: SharedPreferencesManager,
        private val homeworkStorage: HomeworkStorage,
        private val courseService: FintechService
) {


    private val mutableCourseNameState = MutableLiveData<CourseResult>()
    val courseNameState: LiveData<CourseResult> = mutableCourseNameState

    private val mutableMarkTop10State = MutableLiveData<MarksTop10Result>()
    val markTop10State: LiveData<MarksTop10Result> = mutableMarkTop10State

    private val mutableRatingState = MutableLiveData<RatingResult>()
    val ratingState: LiveData<RatingResult> = mutableRatingState

    val isLoading = MutableLiveData<Boolean>()

    fun getCourseName() {
        isLoading.value = true
        netManager.isConnectedToInternet?.let {
            if (it) {
                courseService
                        .getCourse(sharedPreferencesManager.getCookie())
                        .enqueue(object : Callback<Courses> {
                            override fun onFailure(call: Call<Courses>, t: Throwable) {
                                isLoading.value = false
                                mutableCourseNameState.value = Right("Ошибка запроса")
                            }

                            override fun onResponse(call: Call<Courses>, response: Response<Courses>) {
                                if (response.isSuccessful) {
                                    val course = response.body()!!.courses[0]
                                    sharedPreferencesManager.saveCourseInfo(course)
                                    mutableCourseNameState.value = Left(course)
                                } else {
                                    isLoading.value = false
                                    mutableCourseNameState.value = Right("Ошибка ответа")
                                }
                            }

                        })

            } else {
                isLoading.value = false
                mutableCourseNameState.value = Right("Ты забыл включить интернет")
            }
        }

    }

    fun getMarksTop10() {

        if (homeworkStorage.getMarksCount() == 0 || sharedPreferencesManager.isDataOld()) {
            updateStudentsVm()
        } else {
            mutableMarkTop10State.value = Left(homeworkStorage.getMarksTop10())
            makeRating()
        }

    }

    fun updateStudentsVm() {
        if (sharedPreferencesManager.isDataOld()) {
            netManager.isConnectedToInternet?.let {
                if (it) {
                    courseService
                            .getStudentsGroups(sharedPreferencesManager.getCookie(), sharedPreferencesManager.getCourseUrl())
                            .enqueue(object : Callback<List<StudentGroup>> {
                                override fun onFailure(call: Call<List<StudentGroup>>, t: Throwable) {
                                    isLoading.value = false
                                    mutableMarkTop10State.value = Right("Ошибка запроса")
                                }

                                override fun onResponse(
                                        call: Call<List<StudentGroup>>,
                                        response: Response<List<StudentGroup>>
                                ) {
                                    if (response.isSuccessful) {
                                        homeworkStorage.clearMarks()
                                        homeworkStorage.saveMarks(response.body()!![1].students)
                                        mutableMarkTop10State.value = Left(homeworkStorage.getMarksTop10())
                                        sharedPreferencesManager.setMarkSavedTime()
                                        makeRating()
                                    } else {
                                        isLoading.value = false
                                        mutableMarkTop10State.value = Right("Ошибка ответа")
                                    }
                                }

                            })
                } else {
                    isLoading.value = false
                    mutableMarkTop10State.value = Right("Ты забыл включить интернет")
                }
            }
        } else {
            mutableMarkTop10State.value = Left(homeworkStorage.getMarks())
        }
    }


    private fun makeRating() {
        //Сумма набранных баллов
        val profileName = homeworkStorage.getProfileName()
        val sumRate = roundTo2DecimalPlaces(homeworkStorage.getProfileGrade(profileName)).toString()

        //Общее количество занятий
        val lecCount = homeworkStorage.getLectureCount().toString()

        //Позиция студента в рейтинге по сумме набранных баллов (позиция / всего студентов)
        val position = homeworkStorage.getNamePositionInRating(profileName)
        val count = homeworkStorage.getMarksCount()
        val studentInRating = "$position/$count"

        //Количество сданных сданных тестов
        val tests = homeworkStorage.getCountOfType("test_during_lecture")
        val testsAccept = homeworkStorage.getCountOfTypeAccepted("test_during_lecture")
        val acceptedTest = "$testsAccept/$tests"

        //Количество сданных домашних заданий
        val hw = homeworkStorage.getCountOfType("full")
        val hwAccept = homeworkStorage.getCountOfTypeAccepted("full")
        val acceptedHw = "$hwAccept/$hw"

        val rating = listOf(sumRate, lecCount, studentInRating, acceptedTest, acceptedHw)

        mutableRatingState.value = Left(rating)
        isLoading.value = false
    }


    fun updateStudents() {
        if (sharedPreferencesManager.isDataOld()) {
            netManager.isConnectedToInternet?.let {
                if (it) {
                    courseService
                            .getStudentsGroups(sharedPreferencesManager.getCookie(), sharedPreferencesManager.getCourseUrl())
                            .enqueue(object : Callback<List<StudentGroup>> {
                                override fun onFailure(call: Call<List<StudentGroup>>, t: Throwable) {
                                }

                                override fun onResponse(
                                        call: Call<List<StudentGroup>>,
                                        response: Response<List<StudentGroup>>
                                ) {
                                    if (response.isSuccessful) {
                                        homeworkStorage.clearMarks()
                                        homeworkStorage.saveMarks(response.body()!![1].students)
                                        sharedPreferencesManager.setMarkSavedTime()
                                    }
                                }

                            })
                }
            }
        }
    }

    fun updateLectures() {
        netManager.isConnectedToInternet?.let {
            if (it) {
                courseService
                        .getHomeworks((sharedPreferencesManager.getCookie()))
                        .enqueue(object : Callback<Homeworks> {
                            override fun onFailure(call: Call<Homeworks>, t: Throwable) {
                            }
                            override fun onResponse(call: Call<Homeworks>, response: Response<Homeworks>) {
                                if (response.isSuccessful) {
                                    homeworkStorage.clearTables()
                                    homeworkStorage.saveHomeworks(response.body()!!.homeworks)
                                }
                            }
                        })
            }
        }
    }

    fun updateUser() {
        netManager.isConnectedToInternet?.let {
            if (it) {
                courseService
                        .getProfile(sharedPreferencesManager.getCookie())
                        .enqueue(object : Callback<ResponseUser> {
                            override fun onFailure(call: Call<ResponseUser>, t: Throwable) {
                            }

                            override fun onResponse(call: Call<ResponseUser>, response: Response<ResponseUser>) {
                                if (response.isSuccessful) {
                                    val profile = response.body()!!.user
                                    homeworkStorage.reSaveProfile(profile)
                                    getMarksTop10()
                                }
                            }
                        })
            }
        }
    }

    private fun roundTo2DecimalPlaces(d: Double) = BigDecimal(d).setScale(2, BigDecimal.ROUND_HALF_UP).toDouble()

}

typealias CourseResult = Either<Course, String>
typealias MarksTop10Result = Either<List<Mark>, String>
typealias RatingResult = Either<List<String>, String>