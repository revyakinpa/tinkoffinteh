package com.revyakinpa.tinkofffintech.course.performance

import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.revyakinpa.tinkofffintech.DiffCallback
import com.revyakinpa.tinkofffintech.R
import com.revyakinpa.tinkofffintech.UserPerformanceView
import com.revyakinpa.tinkofffintech.storage.Mark

class CoursePerformanceAdapter(var marks: List<Mark>) :
        RecyclerView.Adapter<CoursePerformanceAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_course_performance, parent, false))

    }

    override fun onBindViewHolder(holder: ViewHolder, index: Int) = holder.bind(marks[index])

    override fun getItemCount() = marks.size


    fun updateList(newMarks: List<Mark>) {
        val callback = DiffCallback(marks, newMarks)
        marks = newMarks
        DiffUtil.calculateDiff(callback).dispatchUpdatesTo(this)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val card: UserPerformanceView = view.findViewById(R.id.userPerformance)

        fun bind(mark: Mark) {
            card.setBageValue(mark.mark.toInt())
            card.setUserName(mark.student)
            val colorRange = 0..192
            card.setColorImg(Color.argb(255, (colorRange).random(), (colorRange).random(), (colorRange).random()), PorterDuff.Mode.SRC_IN)
        }
    }
}