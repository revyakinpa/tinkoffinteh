package com.revyakinpa.tinkofffintech.course

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import javax.inject.Inject

class CourseViewModel @Inject constructor(
        app:Application,
        private val model: CourseModel
): AndroidViewModel(app) {

    val courseNameState = model.courseNameState
    val marksTop10State = model.markTop10State
    val ratingState = model.ratingState
    val isLoading = model.isLoading

    fun getCourseName() {
        model.getCourseName()
        model.updateStudents()
        model.updateLectures()
        model.updateUser()
    }

}