package com.revyakinpa.tinkofffintech.course

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.revyakinpa.tinkofffintech.*
import com.revyakinpa.tinkofffintech.course.performance.CoursePerformanceFragment
import com.revyakinpa.tinkofffintech.course.rating.CourseRatingFragment
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_course.*
import javax.inject.Inject

class CourseFragment : DaggerFragment(),
        SwipeRefreshLayout.OnRefreshListener {

    private val PERFORMANCE_TAG = "performance_tag"
    private val RATING_TAG = "rating_tag"
    private lateinit var performanceFragment: CoursePerformanceFragment
    private lateinit var ratingFragment: CourseRatingFragment

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: CourseViewModel

    companion object {
        fun newInstance(): CourseFragment {
            return CourseFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_course, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CourseViewModel::class.java)

        viewModel.getCourseName()

        viewModel.courseNameState.observe(this, Observer {
            when (it) {
                is Left -> {
                    requireActivity().title = it.value.title
                    addCourseFragments()
                }
                is Right -> {
                    text_retry.visibility = View.VISIBLE
                    Toast.makeText(requireContext(), it.value, Toast.LENGTH_SHORT).show()
                }
            }
        })

        viewModel.isLoading.observe(this, Observer {
            when (it) {
                true -> {
                    if (!swiperefresh.isRefreshing) {
                        course_scroll.visibility = View.GONE
                        course_progressbar.visibility = View.VISIBLE
                    }
                }
                false -> {
                    swiperefresh.isRefreshing = false
                    course_progressbar.visibility = View.GONE
                    course_scroll.visibility = View.VISIBLE
                }
            }
        })

        if (savedInstanceState != null) {
            performanceFragment = fragmentManager!!.findFragmentByTag(PERFORMANCE_TAG) as CoursePerformanceFragment
            ratingFragment = fragmentManager!!.findFragmentByTag(RATING_TAG) as CourseRatingFragment
        }
        swiperefresh.setOnRefreshListener(this)

        text_retry.setOnClickListener {
            text_retry.visibility = View.GONE
            viewModel.getCourseName()
        }

    }

    override fun onRefresh() {
        viewModel.getCourseName()
    }


    private fun addCourseFragments() {

        performanceFragment = CoursePerformanceFragment.newInstance()
        ratingFragment = CourseRatingFragment.newInstance()

        fragmentManager?.run {
            beginTransaction()
                    .replace(R.id.fragment_performance, performanceFragment, PERFORMANCE_TAG)
                    .commit()
            beginTransaction()
                    .replace(R.id.fragment_rating, ratingFragment, RATING_TAG)
                    .commit()
        }
    }

}