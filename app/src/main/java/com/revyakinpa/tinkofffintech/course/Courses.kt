package com.revyakinpa.tinkofffintech.course

data class Courses(val courses: List<Course>)

data class Course(val title: String, val url: String)