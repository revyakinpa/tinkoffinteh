package com.revyakinpa.tinkofffintech.events

import com.google.gson.annotations.SerializedName

data class Events(
    val active: List<Event>,
    val archive: List<Event>
)

data class Event(
    val title: String,
    @SerializedName("date_start")
    val dateStart: String,
    @SerializedName("date_end")
    val dateEnd: String,
    @SerializedName("event_type")
    val eventType: EventType?,
    val place: String,
    val url: String,
    @SerializedName("url_external")
    val urlExternal: Boolean
)

data class EventType(
    val name: String,
    val color: String
)