package com.revyakinpa.tinkofffintech.events

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.revyakinpa.tinkofffintech.*
import com.revyakinpa.tinkofffintech.di.FintechService
import com.revyakinpa.tinkofffintech.storage.Event
import com.revyakinpa.tinkofffintech.storage.HomeworkStorage
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class EventListModel @Inject constructor(
        private val netManager: NetManager,
        private val sharedPreferencesManager: SharedPreferencesManager,
        private val homeworkStorage: HomeworkStorage,
        private val eventListService: FintechService
) {

    private val mutableEventListState = MutableLiveData<EventListResult>()
    val eventListState: LiveData<EventListResult> = mutableEventListState

    val isLoading = MutableLiveData<Boolean>()

//    private val ACTIVE_TYPE = "100"
//    private val ARCHIVE_TYPE = "200"

    fun getEventsActive() {
        isLoading.value = true
        mutableEventListState.value = Left(homeworkStorage.getEventsActive())
        isLoading.value = false
    }

    fun getEventsArchive() {
        isLoading.value = true
        mutableEventListState.value = Left(homeworkStorage.getEventsArchive())
        isLoading.value = false
    }


    fun updateEvents(eventListType: String) {
        netManager.isConnectedToInternet?.let {
            if (it) {

                eventListService
                        .getEvents()
                        .enqueue(object : Callback<Events> {
                            override fun onFailure(call: Call<Events>, t: Throwable) {
                                mutableEventListState.value = Right("Ошибка запроса")
                                isLoading.value = false
                            }

                            override fun onResponse(call: Call<Events>, response: Response<Events>) {
                                if (response.isSuccessful) {
                                    homeworkStorage.clearEvents()
                                    homeworkStorage.saveEvents(response.body()!!)
                                    when(eventListType) {
                                        ACTIVE_TYPE -> mutableEventListState.value = Left(homeworkStorage.getEventsActive())
                                        ARCHIVE_TYPE -> mutableEventListState.value = Left(homeworkStorage.getEventsArchive())
                                    }
                                } else {
                                    mutableEventListState.value = Right("Ошибка овтета")
                                }
                                isLoading.value = false
                            }

                        })

            } else {
                isLoading.value = false
                mutableEventListState.value = Right("Ты не включил интернет")
            }
        }
    }
}

typealias EventListResult = Either<List<Event>, String>