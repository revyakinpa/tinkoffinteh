package com.revyakinpa.tinkofffintech.events

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import javax.inject.Inject

class EventViewModel @Inject constructor(
    app: Application,
    private val model: EventModel
) : AndroidViewModel(app) {

    val eventsState = model.eventsState
    val isLoading = model.isLoading

    fun getEvents() {
        model.getEvents()
    }

    fun loadEvents() {
        model.loadEvents()
    }

}