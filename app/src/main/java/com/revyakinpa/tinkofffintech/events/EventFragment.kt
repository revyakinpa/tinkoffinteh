package com.revyakinpa.tinkofffintech.events

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.revyakinpa.tinkofffintech.Left
import com.revyakinpa.tinkofffintech.R
import com.revyakinpa.tinkofffintech.Right
import com.revyakinpa.tinkofffintech.storage.Event
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_event.*


import javax.inject.Inject

class EventFragment : DaggerFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: EventViewModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().setTitle(R.string.title_event)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EventViewModel::class.java)

        viewModel.getEvents()

        viewModel.eventsState.observe(this, Observer<EventsResult> { either ->

            when (either) {
                is Left -> {
                    either.value.let {
                        val fm = requireActivity().supportFragmentManager
                        fm.beginTransaction()
                                .replace(
                                        R.id.fragment_event_active,
                                        EventPreviewFragment.newInstance(it[0] as ArrayList<Event>, "Актуальное")
                                )
                                .replace(
                                        R.id.fragment_event_archive,
                                        EventPreviewFragment.newInstance(it[1] as ArrayList<Event>, "Прошедшие")
                                )
                                .commit()
                    }
                }
                is Right -> {
                    Toast.makeText(requireContext(), either.value, Toast.LENGTH_SHORT).show()
                }
            }

        })


        viewModel.isLoading.observe(this, Observer {
            if (it) {
                if (!swiperefresh.isRefreshing) {
                    cards.visibility = View.GONE
                    progressBar.visibility = View.VISIBLE
                }
            } else {
                cards.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                swiperefresh.isRefreshing = false
            }
        })

        swiperefresh.setOnRefreshListener {
            viewModel.getEvents()
        }
    }


    companion object {
        fun newInstance(): EventFragment {
            return EventFragment()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_event, container, false)
    }


}