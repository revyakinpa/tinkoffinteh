package com.revyakinpa.tinkofffintech.events

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.revyakinpa.tinkofffintech.R
import com.revyakinpa.tinkofffintech.performance.PerformanceItemDecoration
import com.revyakinpa.tinkofffintech.storage.Event
import kotlinx.android.synthetic.main.fragment_event_preview.*
import kotlin.collections.ArrayList

class EventPreviewFragment : Fragment() {

    private lateinit var eventListAdapter: EventListAdapter
    private var events: List<Event> = listOf()

    companion object {

        fun newInstance(events: ArrayList<Event>, typeName: String): EventPreviewFragment {
            return EventPreviewFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList("events", events)
                    putString("typeName", typeName)
                }
            }
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_event_preview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        event_preview_typetext.text = arguments?.getString("typeName", "Мероприятия")

        events = arguments?.getParcelableArrayList<Event>("events") as List<Event>
        event_preview_count.text = "ВСЕ ${events.size}"
        events = getThreeFromList(events)


        val type = when (arguments?.getString("typeName", "Мероприятия")) {
            "Актуальные" -> ACTIVE_TYPE
            "Прошедшие" -> ARCHIVE_TYPE
            else -> ACTIVE_TYPE
        }

        events_recycler.layoutManager = LinearLayoutManager(requireContext())
        eventListAdapter = EventListAdapter(events, null)
        events_recycler.adapter = eventListAdapter
        events_recycler.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))


        event_preview_count.setOnClickListener {
            val intent = Intent(requireContext(), EventListActivity::class.java)
            intent.putExtra("eventType", type)
            requireContext().startActivity(intent)
        }

    }

    private fun getThreeFromList(list: List<Event>): List<Event> {
        val newList = arrayListOf<Event>()
        val right = when {
            list.size < 3 -> list.size
            else -> 3
        }
        val range = 0..(right-1)
        for (i in range) {
            newList.add(list[i])
        }
        return newList

    }
}