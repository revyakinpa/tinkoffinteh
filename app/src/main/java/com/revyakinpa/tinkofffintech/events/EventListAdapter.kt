package com.revyakinpa.tinkofffintech.events

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.revyakinpa.tinkofffintech.DiffCallbackEventList
import com.revyakinpa.tinkofffintech.R
import com.revyakinpa.tinkofffintech.storage.Event

class EventListAdapter(private var events: List<Event>, var eventListInterface: EventlistAdapterInterface?) :
        RecyclerView.Adapter<EventListAdapter.ViewHolder>(){


    interface EventlistAdapterInterface {
        fun onItemClicked(url: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_event_list, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(events[position], eventListInterface)

    override fun getItemCount() = events.size

    fun updateList(newEventList: List<Event>) {
        val callback = DiffCallbackEventList(events, newEventList)
        events = newEventList
        DiffUtil.calculateDiff(callback).dispatchUpdatesTo(this)

    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        private val title: TextView = view.findViewById(R.id.event_name)
        private val type: TextView = view.findViewById(R.id.event_type)
        private val city: TextView = view.findViewById(R.id.event_city)
        private val img: ImageView = view.findViewById(R.id.event_img)
        private val item: ConstraintLayout = view.findViewById(R.id.event_item)

        fun bind(event: Event, eventListInterface: EventlistAdapterInterface?) {
            title.text = event.title
            type.text = event.eventTypeName
            city.text = event.place
            val drawble_img =
                    when(event.eventTypeColor) {
                        "blue" -> R.drawable.ic_event_blue
                        "green" -> R.drawable.ic_event_green
                        "grey" -> R.drawable.ic_event_grey
                        "orange" -> R.drawable.ic_event_orange
                        "purple" -> R.drawable.ic_event_purple
                        else -> R.drawable.ic_event_grey
                    }
            img.setImageResource(drawble_img)

            if (eventListInterface != null) {
                item.setOnClickListener {
                    eventListInterface.onItemClicked(event.url)
                }
            }
        }
    }

}