package com.revyakinpa.tinkofffintech.events

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.revyakinpa.tinkofffintech.R
import kotlinx.android.synthetic.main.fragment_web.*

class WebFragment: Fragment() {


    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var url = arguments?.getString("url", "")
        url = "https://fintech.tinkoff.ru$url"

        webview!!.webViewClient = object : WebViewClient() {

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                view?.loadUrl(request?.url.toString())
                return true
            }

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }

        val webSettings = webview.settings
        webSettings.javaScriptEnabled = true
        webview.loadUrl(url)
        WebView.setWebContentsDebuggingEnabled(false)


    }


    companion object {
        fun newInstance(url: String): WebFragment {
            return WebFragment().apply {
                arguments = Bundle().apply {
                    putString("url", url)
                }
            }
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_web, container, false)
    }

}