package com.revyakinpa.tinkofffintech.events

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.revyakinpa.tinkofffintech.Left
import com.revyakinpa.tinkofffintech.R
import com.revyakinpa.tinkofffintech.Right
import com.revyakinpa.tinkofffintech.performance.PerformanceItemDecoration
import com.revyakinpa.tinkofffintech.storage.Event
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_event_list.*
import javax.inject.Inject

class EventListFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: EventListViewModel

    private lateinit var eventListAdapter: EventListAdapter
    private var searchView: SearchView? = null

    private var events: List<Event> = listOf()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val eventsType = arguments?.getString("eventsType")
        setHasOptionsMenu(true)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EventListViewModel::class.java)

        events_recycler.layoutManager = LinearLayoutManager(requireContext())
        val eventListInterface = object : EventListAdapter.EventlistAdapterInterface{
            override fun onItemClicked(url: String) {
                requireActivity().supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.web_fragment, WebFragment.newInstance(url))
                        .commit()
            }
        }
        eventListAdapter = EventListAdapter(events, eventListInterface)
        events_recycler.adapter = eventListAdapter
        events_recycler.addItemDecoration(DividerItemDecoration(requireContext(),DividerItemDecoration.VERTICAL))


        when (eventsType) {
            ACTIVE_TYPE -> viewModel.getEventsActive()
            ARCHIVE_TYPE -> viewModel.getEventsArchive()
        }


        viewModel.eventListState.observe(this, Observer<EventListResult> { either ->
            when (either) {
                is Left -> {
                    either.value.let {
                        events = it
                        eventListAdapter.updateList(it)
                    }
                }
                is Right -> {
                    Toast.makeText(requireContext(), either.value, Toast.LENGTH_SHORT).show()
                }
            }
        })


        viewModel.isLoading.observe(this, Observer {
            if (it) {
                events_recycler.visibility = View.GONE
                progressBar.visibility = View.VISIBLE
            } else {
                events_recycler.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
            }
        })

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_event_list, container, false)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    companion object {
        fun newInstance(eventsType: String): EventListFragment {
            return EventListFragment().apply {
                arguments = Bundle().apply {
                    putString("eventsType", eventsType)
                }
            }
        }
    }

}