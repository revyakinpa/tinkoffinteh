package com.revyakinpa.tinkofffintech.events

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.revyakinpa.tinkofffintech.R

class EventListActivity : AppCompatActivity() {

    private val FRAGMENT_TAG = "tag_event_list"

    private lateinit var fragment: EventListFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_list)

        val tt = intent.extras.getString("eventType")
        title = when (tt) {
            ACTIVE_TYPE -> "Активные"
            ARCHIVE_TYPE -> "Прошедшие"
            else -> "Мероприятия"
        }

        if (savedInstanceState == null) {
            fragment = EventListFragment.newInstance(tt)
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment, fragment, FRAGMENT_TAG)
                .commitNow()
        } else {
            fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG) as EventListFragment
        }
    }
}

const val ACTIVE_TYPE = "100"
const val ARCHIVE_TYPE = "200"