package com.revyakinpa.tinkofffintech.events

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.revyakinpa.tinkofffintech.*
import com.revyakinpa.tinkofffintech.di.FintechService
import com.revyakinpa.tinkofffintech.lecture.LectureResult
import com.revyakinpa.tinkofffintech.storage.Event
import com.revyakinpa.tinkofffintech.storage.HomeworkStorage
import com.revyakinpa.tinkofffintech.storage.Lecture
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class EventModel @Inject constructor(
    private val netManager: NetManager,
    private val sharedPreferencesManager: SharedPreferencesManager,
    private val homeworkStorage: HomeworkStorage,
    private val eventService: FintechService
) {


    private val mutableEventsState = MutableLiveData<EventsResult>()
    val eventsState: LiveData<EventsResult> = mutableEventsState

    val isLoading = MutableLiveData<Boolean>()

    fun getEvents() {
        isLoading.value = true
        if (homeworkStorage.getEventCount() == 0) {
            loadEvents()
        } else {
            mutableEventsState.value = Left(homeworkStorage.getEvents())
            isLoading.value = false
        }

    }

    fun loadEvents() {
        netManager.isConnectedToInternet?.let {
            if (it) {

                eventService
                    .getEvents()
                    .enqueue(object : Callback<Events> {
                        override fun onFailure(call: Call<Events>, t: Throwable) {
                            mutableEventsState.value = Right("Ошибка запроса")
                            isLoading.value = false
                        }

                        override fun onResponse(call: Call<Events>, response: Response<Events>) {
                            if (response.isSuccessful) {
                                homeworkStorage.clearEvents()
                                homeworkStorage.saveEvents(response.body()!!)
                                mutableEventsState.value = Left(homeworkStorage.getEvents())
                            } else {
                                mutableEventsState.value = Right("Ошибка овтета")
                            }
                            isLoading.value = false
                        }

                    })

            } else {
                isLoading.value = false
                mutableEventsState.value = Right("Ты не включил интернет")
            }
        }
    }

}

typealias EventsResult = Either<List<List<Event>>, String>