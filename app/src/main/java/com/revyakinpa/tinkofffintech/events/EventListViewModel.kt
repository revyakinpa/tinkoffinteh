package com.revyakinpa.tinkofffintech.events

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import javax.inject.Inject

class EventListViewModel @Inject constructor(
        app: Application,
        private val model: EventListModel
) : AndroidViewModel(app) {

    val eventListState = model.eventListState
    val isLoading = model.isLoading

    fun getEventsActive() {
        model.getEventsActive()
    }

    fun getEventsArchive() {
        model.getEventsArchive()
    }

    fun updateEvents(s: String) {
        model.updateEvents(s)
    }


}