package com.revyakinpa.tinkofffintech

import com.google.gson.annotations.SerializedName

data class Homeworks(val homeworks: List<Homework>)

data class Homework(val id: Int, val title: String, val tasks: List<Tasks>)

data class Tasks(val id: Int, val mark: Double, val status: String, val task: TaskTask)

data class TaskTask(
        @SerializedName("deadline_date")
        val deadlineDate: String?,
        @SerializedName("max_score")
        val maxScore: Double,
        val title: String,
        @SerializedName("task_type")
        val taskType: String
)