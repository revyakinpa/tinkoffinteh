package com.revyakinpa.tinkofffintech.profile

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
        app: Application,
        private val model: ProfileModel
) : AndroidViewModel(app) {

    val profileState = model.profileState
    val isLoading = model.isLoading

    fun getUser() {
        model.getUser()
    }

    fun logout() {
        model.logout()
    }
}