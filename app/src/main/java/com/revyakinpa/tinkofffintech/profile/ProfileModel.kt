package com.revyakinpa.tinkofffintech.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.revyakinpa.tinkofffintech.*
import com.revyakinpa.tinkofffintech.di.FintechService
import com.revyakinpa.tinkofffintech.storage.HomeworkStorage
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class ProfileModel @Inject constructor(
        private val netManager: NetManager,
        private val sharedPreferencesManager: SharedPreferencesManager,
        private val homeworkStorage: HomeworkStorage,
        private val profileService: FintechService
) {

    private val mutableProfileState = MutableLiveData<ProfileResult>()
    val profileState: LiveData<ProfileResult> = mutableProfileState

    val isLoading = MutableLiveData<Boolean>()

    fun getUser() {
        isLoading.value = true
        netManager.isConnectedToInternet?.let {
            if (it) {
                profileService
                        .getProfile(sharedPreferencesManager.getCookie())
                        .enqueue(object : Callback<ResponseUser> {
                            override fun onFailure(call: Call<ResponseUser>, t: Throwable) {
                                mutableProfileState.value = Right("Ошибка запроса")
                                isLoading.value = false
                            }

                            override fun onResponse(call: Call<ResponseUser>, response: Response<ResponseUser>) {
                                if (response.isSuccessful) {
                                    val profile = response.body()!!.user
                                    homeworkStorage.reSaveProfile(profile)
                                    mutableProfileState.value = Left(profile)
                                    isLoading.value = false
                                } else {
                                    mutableProfileState.value = Right("Ошибка ответа")
                                }
                            }

                        })
            } else {
                mutableProfileState.value = Right("Ты забыл включить интернет")
                isLoading.value = false
            }
        }
    }

    fun logout() {
        sharedPreferencesManager.revomeCookies()
    }

}

typealias ProfileResult = Either<Profile, String>