package com.revyakinpa.tinkofffintech.profile

import com.revyakinpa.tinkofffintech.ResponseUser
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header

interface ProfileService {
    @GET("user")
    fun getProfile(@Header("Cookie") cookie: String): Call<ResponseUser>
}