package com.revyakinpa.tinkofffintech.profile

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.revyakinpa.tinkofffintech.*
import com.revyakinpa.tinkofffintech.login.LoginActivity
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class ProfileFragment : DaggerFragment() {

    private val fTitle = R.string.title_profile // Заголовок для Toolbar

    companion object {

        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: ProfileViewModel

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().setTitle(fTitle)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProfileViewModel::class.java)
        imageView.clipToOutline = true

        viewModel.getUser()

        viewModel.profileState.observe(this, Observer {
            when(it) {
                is Left -> {
                    val profile = it.value
                    setAvatar(profile.avatar)

                    profile_name.text = "${profile.firstName} ${profile.lastName}"
                    profile_email.text = profile.email
                    profile_city.text = profile.region
                    profile_email_contact.text = profile.email
                    profile_phone.text = profile.phoneMobile

                }
                is Right -> {
                    text_retry.visibility = View.VISIBLE
                    profile_progressbar.visibility = View.GONE
                    profile_layout.visibility = View.GONE
                    Toast.makeText(requireContext(), it.value, Toast.LENGTH_SHORT).show()
                }
            }
        })

        viewModel.isLoading.observe(this, Observer {
            if (it) {
                profile_progressbar.visibility = View.VISIBLE
                text_retry.visibility = View.GONE
                profile_layout.visibility = View.GONE
            } else {
                if (viewModel.profileState.value is Left)  {
                    profile_progressbar.visibility = View.GONE
                    profile_layout.visibility = View.VISIBLE
                }
            }
        })


        text_retry.setOnClickListener {
            viewModel.getUser()
        }

        profile_logout_btn.setOnClickListener {
            viewModel.logout()
            val intent = Intent(requireContext(), LoginActivity::class.java)
            requireContext().startActivity(intent)
            requireActivity().finish()

        }
    }


    private fun setAvatar(url: String?) {
        if (url != null) {
            val imgUrl = "https://fintech.tinkoff.ru$url"
            Picasso
                .get()
                .load(imgUrl)
                .into(imageView)
        } else {
            imageView.setImageResource(R.drawable.unicorn_mobile)
        }
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }



}