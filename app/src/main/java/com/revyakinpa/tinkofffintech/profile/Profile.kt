package com.revyakinpa.tinkofffintech.profile

import com.google.gson.annotations.SerializedName


data class ResponseUser(val user: Profile, val status: String)

data class Profile(
        val birthday: String?,
        val email: String,
        @SerializedName("first_name")
        val firstName: String,
        @SerializedName("last_name")
        val lastName: String,
        @SerializedName("middle_name")
        val middleName: String,
        @SerializedName("phone_mobile")
        val phoneMobile: String?,
        @SerializedName("t_shirt_size")
        val tShirtSize: String?,
        val region: String?,
        val university: String?,
        val faculty: String?,
        val avatar: String?
)