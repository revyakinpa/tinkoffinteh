package com.revyakinpa.tinkofffintech

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.revyakinpa.tinkofffintech.storage.Mark
import java.math.BigDecimal

class ContactAdapter(private var contacts: List<Mark>, var layoutType: Int) :
        RecyclerView.Adapter<ContactAdapter.ViewHolder>(), Filterable {


    private val LINEAR = 0
    private val GRID = 1

    private var contactsSearch: List<Mark>

    init {
        contactsSearch = contacts
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            GRID -> return ViewHolder(inflater.inflate(R.layout.item_contact_grid, parent, false))
            else -> ViewHolder(inflater.inflate(R.layout.item_contact, parent, false))
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, index: Int) = holder.bind(contactsSearch[index])

    fun changeLayoutType() {
        layoutType = if (layoutType == LINEAR) GRID else LINEAR
    }

    override fun getItemCount() = contactsSearch.size

    override fun getItemViewType(position: Int): Int = layoutType

    fun updateList(newContacts: List<Mark>) {
        val callback = DiffCallback(contactsSearch, newContacts)
        contactsSearch = newContacts
        contacts = newContacts
        DiffUtil.calculateDiff(callback).dispatchUpdatesTo(this)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name: TextView = view.findViewById(R.id.contact_name)
        private val score: TextView = view.findViewById(R.id.contact_score)
        private val roundImg: InitialsRoundView = view.findViewById(R.id.initialRound)

        fun bind(mark: Mark) {
            name.text = mark.student
            score.text = roundTo2DecimalPlaces(mark.mark).toString()
            val nameInitial = mark.student
                    .split(' ')
                    .mapNotNull { it.firstOrNull()?.toString() }
                    .reduce { acc, s -> (acc + s) }
            var initial = nameInitial[0].toString()
            if (nameInitial.count() > 1) {
                initial += nameInitial[1].toString()
            }
            roundImg.setInitials(initial)
            val colorRange = 0..192
            roundImg.setRoundColor(
                    Color.argb(255, (colorRange).random(), (colorRange).random(), (colorRange).random())
            ) //от 0 до 192, потому что цвет текста белый, а фон тогда нужен темнее
        }

        private fun roundTo2DecimalPlaces(d: Double) = BigDecimal(d).setScale(2, BigDecimal.ROUND_HALF_UP).toDouble()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    contactsSearch = contacts
                } else {
                    val filteredList = ArrayList<Mark>()
                    for (row in contacts) {
                        if (row.student.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }
                    contactsSearch = filteredList
                }
                val filterResults = Filter.FilterResults()
                filterResults.values = contactsSearch
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                contactsSearch = filterResults.values as List<Mark>
                contactsSearch = contactsSearch.sortedWith(compareByDescending<Mark> { it.mark }.thenBy { it.student })
                notifyDataSetChanged()
            }
        }
    }
}